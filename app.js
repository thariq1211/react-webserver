const express = require('express');
const path = require('path');
const fs = require("fs")
const https = require("https");
const app = express();

app.use(express.static(path.join(__dirname, 'build')));

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

const port = process.env.PORT || "3000"

https.createServer({
  cert: fs.readFileSync(path.join(__dirname, "cert", "localhost.crt")),
  key: fs.readFileSync(path.join(__dirname, "cert", "localhost.key"))
}, app).listen(port, () => {
  console.log("server up in ", port)
});
